# m347-ref-card-08

A project made by Berufsbildungsschule Winterthur.

Autor: Peter Rutschmann

## Reason for the application

The application allows to train the usage of environment variables.

Different environment variables will affect the application.

## Themes for the application

There is a "light" and a "dark" theme available.
Default the application uses the "light" theme. 
Define other themes by the environment variable "REFCARD_THEME".

Easter egg: There is an (incomplete) "blue" theme available. 

## Captcha for the application

There is a recaptcha implemented.
The captcha secret and key has to be defined by environment variables REFCARD_RECAPTCHA_KEY_SITE and REFCARD_RECAPTCHA_KEY_SECRET.

Key and secret must be configured for the host e.g. IP-Adresse.
See: https://www.google.com/recaptcha/admin/create

## Database access

There is access to a database available.

Prepare the sql-database `messagedb`:

```
DROP DATABASE IF EXISTS messagedb;
CREATE DATABASE messagedb;
USE messagedb;
CREATE TABLE message (
   id int NOT NULL AUTO_INCREMENT,
   content varchar(50), 
   author varchar(25),
   origin DATETIME,
   PRIMARY KEY (id)
);
INSERT INTO message (content, author) VALUES
   ('In gravida nulla.', 'Paul Keller'),
   ('Cras purus odio, vestibulum.', 'Celine Maurer'),
   ('Cras sit amet nibh libero', 'Joe Dark'),
   ('Nulla vel metus scelerisque.', 'Carla Bianco');
```
The db-connection-string, db-user und db-password has to be defined by environment variables REFCARD_DB_URL, REFCARD_DB_USER and REFCARD_DB_PASSWORD.

This is an example for the connection-string:
jdbc:mysql://localhost:3306/messagedb?verifyServerCertificate=false&useSSL=false