package ch.bbw.m347refcard08.captcha;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * ReCaptchaValidationService
 * @author Peter Rutschmann
 * @version 24.05.2023
 */
@Service
@Getter
@Slf4j
public class ReCaptchaValidationService {
	@Autowired
	private ReCaptchaProperties captchaSettings;

	private static final String GOOGLE_RECAPTCHA_ENDPOINT = "https://www.google.com/recaptcha/api/siteverify";

	public boolean validateCaptcha(String captchaResponse) {
		RestTemplate restTemplate = new RestTemplate();

		MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<>();
		requestMap.add("secret", captchaSettings.getSecret());
		requestMap.add("response", captchaResponse);

		ReCaptchaResponseType apiResponse = restTemplate.postForObject(GOOGLE_RECAPTCHA_ENDPOINT, requestMap,
				ReCaptchaResponseType.class);
		if (apiResponse == null) {
			log.warn("validateCaptcha: apiResponse is null.");
			return false;
		}
		log.info("validateCaptcha: apiResponse is successfully.");
		return apiResponse.isSuccess();
	}
}
